# Use Alpine Linux for its minimal size
FROM alpine:latest as base

# Install OpenSSH
RUN apk add --no-cache openssh \
    # && `[-d /run/sshd ] || mkdir -p /run/sshd` \
    && sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config \
    && sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/' /etc/ssh/sshd_config \
    && mkdir /run/sshd \
    && ssh-keygen -A

COPY create-user.sh /usr/local/bin/create-user.sh

# Expose the SSH port
EXPOSE 22

# Run sshd daemon
CMD ["/bin/sh", "-c", "/usr/local/bin/create-user.sh && /usr/sbin/sshd -D"]
# CMD ["/usr/sbin/sshd", "-D"]