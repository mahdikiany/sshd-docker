#!/bin/sh
# Check if both USER_NAME and USER_PASS are set
if [ -n "$USER_NAME" ] && [ -n "$USER_PASS" ]; then
    # Create user
    adduser -D "$USER_NAME"
    # Set password
    echo "$USER_NAME:$USER_PASS" | chpasswd
fi